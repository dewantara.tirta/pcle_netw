// ==UserScript==
// @name         Particle CrossChain transfer
// @namespace    http://tampermonkey.net/
// @version      1.1a
// @description  try to take over the world!
// @author       You
// @match        https://wallet.particle.network/*
// @icon         https://www.google.com/s2/favicons?sz=64&domain=particle.network
// @require      https://unpkg.com/rxjs@7.8.1/dist/bundles/rxjs.umd.min.js
// @grant        none
// ==/UserScript==

// CONFIG
const TARGET_ADDRESS = "0x00000"; //0x00000
const TARGET_VALUE = "0.001";
const TOKEN_TO_SEND = "USDG"; //ETH|USDG|USDC
const TARGET_NETWORK = "Berachain bArtio"; // Ethereum Sepolia|Optimism Sepolia|Arbitrum Sepolia|Base Sepolia|Linea Sepolia|Blast Sepolia
const RANDOM_NETWORK = false; // send to random network

// OPTIONS
const WAITING_TIME = 10000; // 10 sec, waiting time to trigger another transaction
const LOADING_ELEMENT = 800; // 0.5 sec,
const SENDING_STALE_TIME = 30; // 15 sec, checking stale time after sending
var staleTimeout;

var mining_start = false;
var last_state = null;
var last_state_date = new Date();

const networks = [
  "Ethereum Sepolia",
  "Optimism Sepolia",
  "Arbitrum Sepolia",
  "Base Sepolia",
  "Linea Sepolia",
  "Blast Sepolia",
  "Berachain bArtio",
];

var stale;
var last_send = false;

const {Observable} = rxjs;

// element
const body = document.querySelector("body");
const send_to = () => document.querySelector("#send_to");
const send_amount = () => document.getElementById("send_amount");
const btn_send = () => document.querySelector("#send > div.ant-form-item.form-footer button");
const popup_action_send = () => document.querySelector(".ant-drawer-body div.bootom-action button");
const popup_title_trx = () => document.querySelector(".ant-drawer-body .transaction-result-container .title");
const popup_close_trx = () => document.querySelector(".ant-drawer-header .ant-drawer-extra span");
const popup_mask = () => document.querySelector(".ant-drawer .ant-drawer-mask");
const btn_send_from_home = () => document.querySelector(".wallet-info-content .operate-content span.micon");
const preview_title = () => document?.querySelector(".header-container .middle .wrap")?.innerText || false;
const btn_preview_send = () => document?.querySelector(".swap-container .content-footer button.swap-btn") || false;
const target_network_el = () => document?.querySelector(".form-content .choose-token.choose-chain .symbol")?.innerText || false
const choose_target_network_btn = () => document?.querySelector(".form-content .choose-chain");
const ant_drawer_title = () => document?.querySelector(".ant-drawer-title")?.innerText || false;
const choose_token_to_send = () => document?.querySelector(".send-main .choose-token")?.innerText || false
const choose_token_to_send_btn = () =>  document?.querySelector(".send-main .choose-token") || false;

const bodyClassList = () => Array.from(body.classList);

const sendPage = () => {
  let classList = bodyClassList();
  return classList.includes("_page_send");
};
const confirmPage = () => {
  let classList = bodyClassList();
  return classList.includes("_page_transferConfirm");
};
const popup = () => {
  let classList = bodyClassList();
  return classList.includes("ant-scrolling-effect");
};
const popup_title = () => {
  return popup_title_trx()?.innerText || false;
};

var btn_preview_timeout = false;
const btn_preview_send_interval = async() => {
  let b = btn_preview_send();
  if (!mining_start) return;

  await delay(2000);
  setTimeout(() => {
    clearTimeout(btn_preview_timeout);
    if (b.disabled) {
      btn_preview_send_interval();
    } else {
      b.click();
    }
  }, 500);
};

function setState(name = null) {
  last_state = name;
  console.log(`state: ${name}`);
  last_state_date = new Date();
}

function isStale() {
  let check_state_time = (new Date() - last_state_date) / 1000;
  if (check_state_time < SENDING_STALE_TIME) return false;
  return true;
}

const getRandomNetwork = () => {
  return networks[Math.floor(Math.random() * networks.length)];
};
var last_choosen_network = TARGET_NETWORK.toLowerCase();

const mockInput = (element, value) => {
  let input = element;
  let lastValue = input.value;
  input.value = value;
  let event = new Event("input", { bubbles: true });
  event.simulated = true;
  let tracker = input._valueTracker;
  if (tracker) {
    tracker.setValue(lastValue);
  }
  input.dispatchEvent(event);
};

const classWatcher = (element, callback) => {
  const observer = new MutationObserver((ev) => {
    if (typeof callback === "function") callback();
  });

  observer.observe(element, {
    attributes: true,
  });
  return observer;
};

function delay(t, val) {
  return new Promise(resolve => setTimeout(resolve, t, val));
}

Promise.prototype.delay = function(t) {
  return this.then(function(val) {
      return delay(t, val);
  });
}












const setAmountAction = () => { 
  let s = send_to();
  let a = send_amount();
  mockInput(s, TARGET_ADDRESS);
  mockInput(a, TARGET_VALUE);
  return delay(500, 'ok');
}

const setTokenToSendAction  = async () => {
  console.log('choose token to send');
  return new Promise(async () => {
    choose_token_to_send_btn().click();
    await delay(2000);
    let tk =document.querySelectorAll('.scrollContainer .token-list .token-item .name');
    let res = false;
    Array.from(tk).forEach(i => {
      if(i.innerText.toLowerCase() == TOKEN_TO_SEND.toLocaleLowerCase()){
        res = i.parentElement.parentElement.parentElement;
      }
    });
    await delay(500);
    res.click();
    return delay(500);
  });
};



const bodyCallback = async () => {
  if (!mining_start) return;

  let state_send = sendPage();
  let state_popup = popup();
  await delay(2000, 'ok');

  switch (true) {
      // sending state
      case state_send && !state_popup:
        if (
          last_state == "universal_preview_trx" ||
          last_state == "input_send"
        ) {
          let back = document.querySelector(".back-btn");
          setState("input_send");
          back.click();
          return;
        }


        // check token to send
        if (choose_token_to_send() !== TOKEN_TO_SEND) {
          await setTokenToSendAction();
        }

        await setAmountAction();
        console.log(`after set amount action`)

        if (RANDOM_NETWORK && last_state != "choose_network") {
          await delay(500);
          last_choosen_network = getRandomNetwork().toLowerCase();
        }

        let tgt_network = target_network_el();
        console.log(tgt_network.toLowerCase(), last_choosen_network);
        // check target network
        if (
          !!tgt_network &&
          tgt_network.toLowerCase() !== last_choosen_network
        ) {
        console.log(`dooooooooo thiiiiiiiiiissssssssss`);
          await delay(500);
          choose_target_network_btn().click();
          return;
        }


        delay(500);
        let btn = document.querySelector('.ant-form .form-footer button[type="submit"]');
        console.log(btn);
        btn.click();
        setState("input_send");

        break;

      // preview universal trx
      case confirmPage() && preview_title() == "Preview":
        let ls = last_state;
        setState("universal_preview_trx");
        if (ls == "input_send") {
          last_send = new Date();
          btn_preview_send_interval();
        } else {
          let back = document.querySelector(".back-btn");
          back.click();
        }
        break;

      case state_popup && popup_title() == "Transaction":
        setState("trx_success");
        let m = popup_mask();
        m.click();
        break;

      // choose network
      case state_popup && ant_drawer_title() == "Networks":
        setState("choose_network");
        let items = document.querySelectorAll(".ant-drawer-body .list .item");
        Array.from(items).forEach((i) => {
          let name = i.querySelector(".name").innerText.toLowerCase();
          if (name == last_choosen_network) {
            console.log(i);
            i.click();
          }
        });
        let ww = popup_mask();
        ww.click();
        break;

      //home
      default:
        setState("home");
        await delay(WAITING_TIME);
          let b = btn_send_from_home();
          b.click();
        break;
  }
  console.log(`state: ${last_state}`);
};

async function listenStale() {
  setTimeout(function () {
    if (!mining_start) return;
    if (isStale()) {
      console.log(`stale ‼️‼️`);
      let back = document.querySelector(".back-btn");
      if (back) back.click();
      let mask = popup_mask();
      if (mask) mask.click();

    }
    clearTimeout(stale);
    stale = listenStale();
  }, 1000);
}

const createStartButton = () => {
  const btn = document.createElement("button");
  btn.innerHTML = "Start Bot";
  btn.id = "start-bot";
  btn.classList.add("ant-btn", "css-var-rab", "ant-btn-dangerous");
  btn.style.position = "fixed";
  btn.style.bottom = "10px";
  btn.style.left = "10px";

  btn.addEventListener("click", function () {
    mining_start = !mining_start;
    let str = "Start Bot";
    if (mining_start) {
      str = "Stop Bot";
    }

    this.innerHTML = str;
    console.log(`mining_start, ${mining_start}`);
    let b = btn_send_from_home();
    b.click();
    if (stale) clearTimeout(stale);
    var stale = listenStale();
  });
  body.append(btn);
};

(function async() {
  "use strict";
  console.log("wallet detected");
  createStartButton();

  classWatcher(body, () => {
    bodyCallback();
  });
})();
